package com.afs.restapi.mapper;

import com.afs.restapi.dto.*;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static CompanyResponse toResponseDto(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeeSize(company.getEmployees().size());
        return companyResponse;
    }

    public static Company toEntity(CompanyUpdateRequest requestDto) {
        Company company = new Company();
        BeanUtils.copyProperties(requestDto, company);
        return company;
    }
}
