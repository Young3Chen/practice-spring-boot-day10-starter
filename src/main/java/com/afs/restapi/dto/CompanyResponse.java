package com.afs.restapi.dto;

import com.afs.restapi.entity.Employee;

import javax.persistence.*;
import java.util.List;

public class CompanyResponse {
    private Integer id;
    private String name;
    private Integer employeeSize;

    public CompanyResponse(){}
    public CompanyResponse(Integer id, String name, Integer employeeSize) {
        this.id = id;
        this.name = name;
        this.employeeSize = employeeSize;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmployeeSize() {
        return employeeSize;
    }

    public void setEmployeeSize(Integer employeeSize) {
        this.employeeSize = employeeSize;
    }
}
