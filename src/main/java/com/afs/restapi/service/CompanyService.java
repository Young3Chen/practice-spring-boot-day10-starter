package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponseDto;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.CompanyUpdateRequest;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.CompanyMapper.toEntity;
import static com.afs.restapi.mapper.CompanyMapper.toResponseDto;
import static java.util.stream.Collectors.toList;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyResponse> getAll() {
        return companyRepository.findAll()
                .stream()
                .map(CompanyMapper::toResponseDto)
                .collect(toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        return companyRepository.findAll(PageRequest.of(page, pageSize)).toList()
                .stream()
                .map(CompanyMapper::toResponseDto)
                .collect(toList());
    }

    public CompanyResponse findById(Integer companyId) {
        return toResponseDto(companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new));
    }

    public CompanyResponse create(CompanyRequest company) {
        Company companyToSave = toEntity(company);
        return toResponseDto(companyRepository.save(companyToSave));
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyUpdateRequest updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }
        return toResponseDto(companyRepository.save(company));
    }

    public List<EmployeeResponseDto> getEmployees(Integer companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees()
                .stream()
                .map(EmployeeMapper::toResponseDto)
                .collect(toList());
    }
}
