package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeRequestDto;
import com.afs.restapi.dto.EmployeeResponseDto;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeRequestDto requestDto) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(requestDto, employee);
        return employee;
    }

    public static EmployeeResponseDto toResponseDto(Employee employee) {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        BeanUtils.copyProperties(employee, employeeResponseDto);
        return employeeResponseDto;
    }

    public static Employee toEntity(EmployeeUpdateRequest requestDto) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(requestDto, employee);
        return employee;
    }
}
