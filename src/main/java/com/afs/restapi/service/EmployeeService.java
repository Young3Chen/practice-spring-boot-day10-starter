package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeRequestDto;
import com.afs.restapi.dto.EmployeeResponseDto;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toEntity;
import static com.afs.restapi.mapper.EmployeeMapper.toResponseDto;
import static java.util.stream.Collectors.toList;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponseDto> findAll() {
        return employeeRepository.findAllByStatusTrue()
                .stream()
                .map(EmployeeMapper::toResponseDto)
                .collect(toList());
    }

    public EmployeeResponseDto update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee toUpdate = toEntity(employeeUpdateRequest);
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return toResponseDto(employee);
    }

    public EmployeeResponseDto findById(int id) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return toResponseDto(employee);
    }

    public List<EmployeeResponseDto> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender)
                .stream()
                .map(EmployeeMapper::toResponseDto)
                .collect(toList());
    }

    public List<EmployeeResponseDto> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList()
                .stream()
                .map(EmployeeMapper::toResponseDto)
                .collect(toList());
    }

    public EmployeeResponseDto insert(EmployeeRequestDto employee) {
        Employee entity = toEntity(employee);
        System.out.println("111" + entity.toString());
        return toResponseDto(employeeRepository.save(entity));
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
