package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;

public class CompanyUpdateRequest {
    String name;

    public CompanyUpdateRequest(String name) {
        this.name = name;
    }
    public CompanyUpdateRequest(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
