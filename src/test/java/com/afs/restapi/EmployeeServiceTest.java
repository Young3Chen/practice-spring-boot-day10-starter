package com.afs.restapi;

import com.afs.restapi.dto.EmployeeRequestDto;
import com.afs.restapi.dto.EmployeeResponseDto;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.afs.restapi.mapper.EmployeeMapper.toResponseDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

     static void assertionsAttribute(EmployeeResponseDto employeeResponse1,EmployeeResponseDto employeeResponse2){
         Assertions.assertEquals(employeeResponse1.getId(), employeeResponse2.getId());
         Assertions.assertEquals(employeeResponse1.getAge(), employeeResponse2.getAge());
         Assertions.assertEquals(employeeResponse1.getName(), employeeResponse2.getName());
         Assertions.assertEquals(employeeResponse1.getGender(), employeeResponse2.getGender());
         Assertions.assertEquals(employeeResponse1.getCompanyId(), employeeResponse2.getCompanyId());
     }


    @Test
    void should_return_all_employees_when_find_all_given_employees() {
        // given
        Employee employee = new Employee(1, "Susan", 22, "Female", 7000, 100);
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee);
        given(employeeRepository.findAllByStatusTrue()).willReturn(employees);

        // when
        List<EmployeeResponseDto> result = employeeService.findAll();

        // should
        assertThat(result, hasSize(1));
        assertionsAttribute(result.get(0), toResponseDto(employee));
    }



    @Test
    void should_update_only_age_and_salary_when_update_given_employee() {
        // given
        String originalName = "Susan";
        Integer newAge = 23;
        String originalGender = "Female";
        Integer newSalary = 10000;
        Employee toUpdateEmployee = new Employee(1, "Tom", newAge, "Male", newSalary, 100);
        EmployeeUpdateRequest employeeRequestDto = new EmployeeUpdateRequest(23, 20000);
        given(employeeRepository.findByIdAndStatusTrue(1)).willReturn(
                Optional.of(new Employee(1, originalName, 22, originalGender, 5000, 100))
        );

        // when
        EmployeeResponseDto updatedEmployee = employeeService.update(1, employeeRequestDto);

        // should
        assertThat(updatedEmployee.getName(), equalTo(originalName));
        assertThat(updatedEmployee.getAge(), equalTo(newAge));
        assertThat(updatedEmployee.getGender(), equalTo(originalGender));
    }
}
